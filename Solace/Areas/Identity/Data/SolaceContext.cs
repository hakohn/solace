﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Solace.Models
{
    public partial class SolaceContext : IdentityDbContext<ApplicationUser>
    {
        public virtual DbSet<VideoGames> VideoGames { get; set; }


        public SolaceContext()
        {
        }
        public SolaceContext(DbContextOptions<SolaceContext> options) : base(options)
        {
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=.;Database=solace;Trusted_Connection=True;MultipleActiveResultSets=true");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<VideoGames>(entity =>
            {
                entity.HasIndex(e => e.Id)
                    .HasName("IX_VideoGames");

                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.Developer)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Genre)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Platform)
                    .IsRequired()
                    .HasMaxLength(50);
            });
        }
    }
}
