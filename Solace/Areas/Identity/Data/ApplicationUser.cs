﻿using Microsoft.AspNetCore.Identity;

namespace Solace.Models
{
    /// <summary>
    /// The user data and profile for the application.
    /// </summary>
    public class ApplicationUser : IdentityUser
    {
    }
}
