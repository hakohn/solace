﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Solace.Models
{
    public partial class VideoGames
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Developer { get; set; }

        public string Genre { get; set; }

        public string Platform { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy}")]
        [Display(Name = "Release date")]
        public DateTime ReleaseDate { get; set; }
    }
}
