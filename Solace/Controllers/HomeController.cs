﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Solace.Models;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Solace.Controllers
{
    public class HomeController : Controller
    {
        #region Protected Members

        /// <summary>
        /// The scoped applicatuin context.
        /// </summary>
        protected SolaceContext mContext;
        /// <summary>
        /// The manager for handling user creation, deletion, searching, roles etc.
        /// </summary>
        protected UserManager<ApplicationUser> mUserManager;
        /// <summary>
        /// The manager for handling signing in and out for our users.
        /// </summary>
        protected SignInManager<ApplicationUser> mSignInManager;

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="context"> The injected context. </param>
        /// <param name="userManager"> The injected userManager. </param>
        /// <param name="signInManager"> The injected signInManager. </param>
        public HomeController(
            SolaceContext context,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            mContext = context;
            mUserManager = userManager;
            mSignInManager = signInManager;
        }

        #endregion

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [Route("create")]
        public async Task<IActionResult> CreateUserAsync()
        {
            await Task.Delay(0);
            return Content("User was created", "text/html");
        }
    }
}
